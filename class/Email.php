<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '/Applications/XAMPP/xamppfiles/htdocs/sendmail/PHPMailer/PHPMailer.php';
require '/Applications/XAMPP/xamppfiles/htdocs/sendmail/PHPMailer/Exception.php';
require '/Applications/XAMPP/xamppfiles/htdocs/sendmail/PHPMailer/SMTP.php';

class Email{
    public function __contruct(){
        parent::__construct();
    }

    public function mailone($email){
        $mensaje = '<html>
        <head>
            <meta charset="UTF-8">
        </head>
        <body>
            <div marginwidth="0" marginheight="0" style="font:14px/20px Helvetica,Arial,sans-serif;margin:0;padding:75px 0 75px 0;text-align:center;background-color:#eeeeee">
                <center>
                    <table border="0" cellpadding="20" cellspacing="0" height="100%" width="100%" style="background-color:#eeeeee">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;border-radius:6px;background-color:none">
                                        <tbody>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;border-radius:6px;background-color:#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" valign="top" style="line-height:150%;font-family:Helvetica;font-size:14px;color:#333333;padding:20px">
                                                                    <p style="padding:0 0 10px 0">Este es un email de ejemplo con una plantilla predeterminada</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </center>
            </div>
        </body>
        </html>';

        $mail = new PHPMailer(true); 

        // Configuramos el protocolo SMTP con autenticación
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "tls"; 

        // Puerto de escucha del servidor
        $mail->Port = 587;

        // Dirección del servidor SMTP
        $mail->Host = 'smtp.gmail.com';

        // Usuario y contraseña para autenticación en el servidor
        $mail->Username   = "email@gmail.com";
        $mail->Password = "password";

        // Dirección de correo del remitente
        $mail->From = "direccion@remitente.com";

        // Nombre del remitente
        $mail->FromName = "Remitente";

        //$mail->AddAddress('wbeltran@wolfsellers.com');
        $mail->AddAddress($email);

        $mail->Subject = "Prueba de envío de mail";

        // Añadimos el contenido al mail creado 
        $mail->isHTML(true);
        $mail->Body = $mensaje;

        // Activo condificacción utf-8
        $mail->CharSet = 'UTF-8';

        // Enviamos el correo
        $mail->Send();
    }

    public function test(){
        $message = '<h1>Prueba de funcion</h1>';
        return $message;
    }
}
